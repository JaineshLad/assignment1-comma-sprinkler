## Comma Sprinkler


As practice will tell you, the English rules for comma placement are complex, frustrating, and often ambiguous.
Many people, even the English, will, in practice, ignore them, and, apply custom rules, or, no rules, at all. Doctor Comma Sprinkler 
solved this issue by developing a set of rules that sprinkles commas in a sentence with no ambiguity and little simplicity. In this problem 
you will help Dr. Sprinkler by producing an algorithm to automatically apply her rules.
Dr. Sprinkler’s rules for adding commas to an existing piece of text are as follows:

If a word anywhere in the text is preceded by a comma, find all occurrences of that word in the text, and put a comma 
before each of those occurrences, except in the case where such an occurrence is the first word of a sentence or already preceded by a comma.
If a word anywhere in the text is succeeded by a comma, find all occurrences of that word in the text, and put a comma after each of those 
occurrences, except in the case where such an occurrence is the last word of a sentence or already succeeded by a comma.

Apply rules 1 and 2 repeatedly until no new commas can be added using either of them.

As an example, consider the text
please sit spot. sit spot, sit. spot here now here.

Because there is a comma after spot in the second sentence, a comma should be added after spot in the third sentence 
as well (but not the first sentence, since it is the last word of that sentence). Also, because there is a comma before the word sit 
in the second sentence, one should be added before that word in the first sentence (but no comma is added before the word sit beginning 
the second sentence because it is the first word of that sentence). Finally, notice that once a comma is added after spot in the third sentence, 
there exists a comma before the first occurrence of the word here. Therefore, a comma is also added before the other occurrence of the word here. 
There are no more commas to be added so the final result is

please, sit spot. sit spot, sit. spot, here now, here.

Input
The input contains one line of text, containing at least 2 characters and at most 1000000 characters. Each character is either a lowercase letter, a comma, a period, or a space. We define a word to be a maximal sequence of letters within the text. The text adheres to the following constraints:

The text begins with a word.
Between every two words in the text, there is either a single space, a comma followed by a space, or a period followed by a space (denoting the end of a sentence and the beginning of a new one).
The last word of the text is followed by a period with no trailing space.

Output
Display the result after applying Dr. Sprinkler’s algorithm to the original text.

***EXAMPLE***

Sample Input 1	
please sit spot. sit spot, sit. spot here now here.

Sample Output 1
please, sit spot. sit spot, sit. spot, here now, here.

Sample Input 2	
one, two. one tree. four tree. four four. five four. six five.

Sample Output 2
one, two. one, tree. four, tree. four, four. five, four. six five.


*************END**********************


## Install Project

To Build and run this project you need to install Visual Studio 2017 or higher.

open project .sln and and click build and run project.

**1.
Select Input Text File fron Windows Explorer.**

**2.
After selecting file click on **"show output"** button.**



**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---