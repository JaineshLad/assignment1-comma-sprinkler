﻿//Group 3
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Input;

namespace CommaSprinkler
{
    public class ProcessFile : ICommand
    {
        private readonly VM vm;
        private string fileContent;

        private readonly List<string> precedesArray = new List<string>();
        private readonly List<string> followsArray = new List<string>();

        private readonly Output output = new Output();
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return !string.IsNullOrEmpty(vm.FileName);
        }

        public ProcessFile(VM vm)
        {
            this.vm = vm;
            vm.PropertyChanged += RaiseCanExecuteChanged;
        }

        private void RaiseCanExecuteChanged(object sender, PropertyChangedEventArgs e)
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

        public void Execute(object parameter)
        {
            fileContent = File.ReadAllText(vm.FileName);
            string result = fileContent;
            vm.Input = result;
            do
            {
                var temp = SprinkleCommas(result);
                if (temp == result)
                {
                    break;
                }
                result = temp;
            } while (true);
            vm.Output = result;
        }

        private string SprinkleCommas(string source)
        {
            precedesArray.Clear();
            followsArray.Clear();
            output.GetSprinkleCommas(source, precedesArray, followsArray);
            var result = output.GenerateOutPut(source, precedesArray, followsArray);
            return result;
        }
    }
}
