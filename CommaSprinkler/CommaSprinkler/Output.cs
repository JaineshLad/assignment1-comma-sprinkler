﻿//Group 3
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommaSprinkler
{
    public class Output
    {
        public void GetSprinkleCommas(string input, List<string> precedesArray, List<string> followsArray)
        {
            string[] sentences = input.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            if (sentences.Length < 1) return;

            foreach (string sentence in sentences)
            {
                if (sentence.Contains(','))
                {
                    string[] sentencearray = sentence.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < sentencearray.Length; i++)
                    {
                        string phrase = sentencearray[i];
                        if (i < sentencearray.Length - 1)
                        {
                            string[] words = phrase.Split(' ');
                            if (!followsArray.Any(p => p == words.Last()))
                            {
                                followsArray.Add(words.Last());
                            }
                        }
                        if (i > 0)
                        {
                            string[] precedingWith = phrase.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            if (precedingWith.Length > 0)
                            {
                                if (precedingWith.First() != null && !precedesArray.Any(p => p == precedingWith.First()))
                                {
                                    precedesArray.Add(precedingWith.First());
                                }

                            }

                        }
                    }
                }
            }
        }

        public string GenerateOutPut(string input, List<string> precedesArray, List<string> followsArray)
        {
            StringBuilder finalString = new StringBuilder();
            string[] words = input.Split(new[] { ' ' },StringSplitOptions.RemoveEmptyEntries);
            foreach (string word in words)
            {
                if (word.EndsWith(",") || word.EndsWith("."))
                { finalString.Append(word + " "); }
                else
                {
                    bool handled = false;
                    foreach (string item in followsArray)
                    {
                        if (word.Contains(item))
                        {
                            finalString.Append(word + ", ");
                            handled = true;
                        }
                    }
                    if (!handled)
                    {
                        finalString.Append(word + " ");
                    }
                }
            }
            string previousWord = null;
            words = finalString.ToString().Split(' ');
            finalString.Clear();
            foreach (string word in words)
            {
                bool exists = false;
                foreach (string item in precedesArray)
                {
                    if (item == word.TrimEnd(',', '.'))
                    {
                        exists = true;
                    }
                }
                if (exists)
                {
                    if (previousWord != null && !previousWord.Contains('.') && !previousWord.Contains(','))
                    {
                        finalString.Append(previousWord + ", ");
                    }
                    else
                    {
                        finalString.Append(previousWord + " ");
                    }
                }
                else
                {
                    if (previousWord != null)
                    {
                        finalString.Append(previousWord + " ");
                    }
                }
                previousWord = word;
            }
            finalString.Append(previousWord);
            return finalString.ToString();
        }

    }
}

