﻿//Group 3
using Microsoft.Win32;
using System;
using System.Windows.Input;

namespace CommaSprinkler
{

    public class SelectFile : ICommand
    {
        private readonly VM vm;

        public SelectFile(VM vm)
        {
            this.vm = vm;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                vm.FileName = openFileDialog.FileName;
                vm.Input = "";
                vm.Output = "";
            }
        }
    }
}
