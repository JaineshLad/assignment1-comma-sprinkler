﻿//Group 3
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace CommaSprinkler
{
    public class VM : INotifyPropertyChanged
    {        
        private string fileName;
        private string output;
        private string input; 
        public ICommand SelectFileCommand
        {
            get; private set;
        }
        public ICommand ProcessFileCommand
        {
            get; private set;
        }
        public string FileName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                NotifyChanged();
            }
        }
        public string Input
        {
            get { return input; }
            set
            {
                input = value;
                NotifyChanged();
            }
        }
        public string Output
        {
            get { return output; }
            set
            {
                output = value;
                NotifyChanged(); }
        }

        public VM()
        {

            SelectFileCommand = new SelectFile(this);
            ProcessFileCommand = new ProcessFile(this);
        }
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
